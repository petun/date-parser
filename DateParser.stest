#!/bin/env stest
<?php
# test framework https://github.com/parf/spartan-test

; include "DateParser.php";
; include "HB.php";

# dp - date parser ; php7 compatible syntax
; $dp = fn(/* string| int */ $date, array $opts = []) => HB::dtParse($date, $opts);

# delimited numbers
$dp("07/04/1973");
    19730704;
// D/M/Y format
$dp("07/04/1973", ["dmy" => 1]);
    19730407;
# US date format is default
$dp("07-04-1973");
    19730704;
$dp("07.04.1973");
    19730704;
$dp("7/4/1973");
    19730704;
$dp("7/4/73");
    19730704;
$dp("07/04/73");
    19730704;
$dp("1973-07-04");
    19730704;
$dp("1973-7-4");
    19730704;

# partial month/year
$dp("07/1973");
    19730700;
$dp("07/73");
    19730700;

# numbers only
$dp("73");
    19730000;
$dp("1973");
    19730000;
$dp("197307");
    19730700;
$dp("19730704");
    19730704;

# month as string
# mon day year
$dp("Jul 4, 1973");
    19730704;
$dp("July 4, 1973");
    19730704;
$dp("July 4, 73");
    19730704;
$dp("July 4 1973");
    19730704;
$dp("July 4 73");
    19730704;
$dp("July4 73");
    19730704;

# day mon year
$dp("4 Jul, 1973");
    19730704;
$dp("4 July, 1973");
    19730704;
$dp("4 July, 73");
    19730704;
$dp("4 July 1973");
    19730704;
$dp("4 July 73");
    19730704;

# week_day, mon day year
$dp("Friday, Jul 4 1973");
    19730704;
$dp("Fri, July 4 1973");
    19730704;

# year mon d
$dp("1973 Jul 4");
    19730704;
# year day mon
$dp("1973 4 Jul");
    19730704;

# partial: mon year
$dp("Jul 1973");
    19730700;
$dp("Jul1973");
    19730700;
$dp("July 1973");
    19730700;
$dp("July1973");
    19730700;

$dp("Jul 73");
    19730700;
$dp("Jul73");
    19730700;
$dp("July 73");
    19730700;
$dp("July73");
    19730700;

# partial: year mon
$dp("1973 Jul");
    19730700;
$dp("1973Jul");
    19730700;
$dp("1973 July");
    19730700;
$dp("1973July");
    19730700;

$dp("73 Jul");
    19730700;
$dp("73Jul");
    19730700;
$dp("73 July");
    19730700;
$dp("72July");
    19720700;

# Unix timestamp()
$dp(1469918475);
    20160730;
