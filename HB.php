<?php


class HB
{
    private const DAY_NAMES = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sun',
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Set'
    ];

    private const MONTH_NAMES = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];

    private const MONTH_SHORT_NAMES = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ];

    private const DATE_NUMERIC = 1;
    private const DATE_WITH_MONTH_NAMES = 2;
    private const STANDARD_DATE = 3;

    public static function dtParse($date, $opts)
    {
        $format = self::detectFormat($date);
        $dateResult = null;
        switch ($format) {
            case self::DATE_NUMERIC:
                $dateResult = self::fromNumber($date);
                break;
            case self::DATE_WITH_MONTH_NAMES:
                $dateResult = self::fromMonthName($date);
                break;
            case self::STANDARD_DATE:
                $dateResult = self::fromStandard($date, $opts);
                break;
        }

        return $dateResult->getResult();
    }

    private static function detectFormat($date): int
    {
        if (is_numeric($date)) {
            return self::DATE_NUMERIC;
        }

        $monthNamesRegex = implode('|', array_merge(self::MONTH_NAMES, self::MONTH_SHORT_NAMES));
        if (preg_match('/' . $monthNamesRegex . '/i', $date)) {
            return self::DATE_WITH_MONTH_NAMES;
        }

        if (preg_match('/[\.\/-]/', $date)) {
            return self::STANDARD_DATE;
        }

        throw new \Exception('Date format not detected');
    }

    /**
     * @param $date
     * @return DateResult|false
     */
    public static function fromNumber($date): DateResult
    {
        $length = strlen($date);

        $map = [
            2 => ['y', DateResult::YEAR],
            4 => ['Y', DateResult::YEAR],
            6 => ['Ym', DateResult::YEAR_MONTH],
            8 => ['Ymd', DateResult::FULL],
            10 => ['U', DateResult::FULL],
        ];

        if (array_key_exists($length, $map)) {
            $date = DateTime::createFromFormat($map[$length][0], $date);
            return new DateResult($date, $map[$length][1]);
        }

        return false;
    }

    /**
     * @param $date
     * @return DateResult|false
     */
    private static function fromStandard($date, array $opts)
    {
        $date = trim(preg_replace('/\.|\/|-/', ' ', $date, -1, $count));

        if ($count === 2) {
            if (isset($opts['dmy'])) {
                $fullFormats = [
                    'd m y',
                    'd m Y',
                ];
            } else {
                $fullFormats = [
                    'm d y',
                    'm d Y',
                ];
            }

            $fullFormats = array_merge($fullFormats, [
                'Y m d',
                'Y n j',
            ]);

            return self::matchPattern($fullFormats, $date, DateResult::FULL);
        }

        if ($count === 1) {
            $partialFormats = [
                'm y',
                'm Y',
            ];
            return self::matchPattern($partialFormats, $date, DateResult::YEAR_MONTH);
        }

        return false;
    }

    private static function fromMonthName($date)
    {
        $dateNamesRegex = implode('|', self::DAY_NAMES);
        $date = trim(preg_replace('/' . $dateNamesRegex . '|,/i', '', $date));
        $date = str_replace(self::MONTH_NAMES, self::MONTH_SHORT_NAMES, $date);

        $monthRegex = implode('|', self::MONTH_SHORT_NAMES);
        if (preg_match('/(' . $monthRegex . ')(\d+)/i', $date)) {
            $date = preg_replace('/(' . $monthRegex . ')(\d+)/i', '${1} ${2}', $date);
        }

        if (substr_count($date, ' ') === 2) {
            $time = strtotime($date);
            if (is_int($time)) {
                return new DateResult(DateTime::createFromFormat('U', $time), DateResult::FULL);
            }

            return self::matchPattern([
                'Y M j'
            ], $date, DateResult::FULL);
        }

        $date = str_replace(' ', '', $date);
        return self::matchPattern([
            'My',
            'yM',
            'YM',
            'MY',
        ], $date, DateResult::YEAR_MONTH);
    }

    /**
     * @param array $patterns
     * @param $date
     * @param string $format
     * @return DateResult|false
     */
    private static function matchPattern(array $patterns, $date, string $format)
    {
        foreach ($patterns as $pattern) {
            $parsedDate = DateTime::createFromFormat($pattern, $date);
            if ($parsedDate instanceof DateTime) {
                return new DateResult($parsedDate, $format);
            }
        }
    }
}


class DateResult
{

    public const FULL = 'Ymd';
    public const YEAR_MONTH = 'Ym';
    public const YEAR = 'Y';

    protected string $format;
    protected DateTime $date;

    public function __construct(DateTime $date, string $format)
    {
        $this->date = $date;
        $this->format = $format;
    }

    public function getResult(): int
    {
        return (int)str_pad($this->date->format($this->format), 8, '0');
    }
}
