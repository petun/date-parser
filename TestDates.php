<?php

require_once "vendor/autoload.php";
require_once "HB.php";


class TestDates extends \PHPUnit\Framework\TestCase {


    /**
     * @dataProvider dateProvider
     * @param $date
     * @param $expected
     */
    public function testDate($date, $opts, $expected)
    {
        $result = HB::dtParse($date, $opts);
        self::assertEquals($expected, $result);
    }

    public static function dateProvider()
    {
        return [
            [
                'date' => '1973',
                'opts' => [],
                'expected' => 19730000,
            ],
            [
                'date' => '07/04/1973',
                'opts' => [
                    "dmy" => 1
                ],
                'expected' => 19730407,
            ],
            [
                'date' => 'July4 73',
                'opts' => [],
                'expected' => 19730704,
            ],
            [
                'date' => '7/4/73',
                'opts' => [],
                'expected' => 19730704,
            ],
            [
                'date' => '07/04/1973',
                'opts' => [],
                'expected' => 19730704,
            ],
            [
                'date' => '1973 Jul',
                'opts' => [],
                'expected' => 19730700,
            ],
            [
                'date' => 'Jul 1973',
                'opts' => [],
                'expected' => 19730700,
            ]
        ];
    }
}
